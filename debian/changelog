node-yarnpkg (1.22.4-4) UNRELEASED; urgency=medium

  * Team upload

  [ Pirate Praveen ]
  * Drop node-babel-core and most of babel 6 build dependencies
    and build with babel 7 (gulp-babel/babel-loader 8.x)
  * Add back transform-inline-imports-commonjs to fix regression
   (Closes: #960120)
  * Add back node-babel-plugin-transform-strict-mode from babel 6

  [ Paolo Greppi ]
  * Avoid misleading error when webpack fails

  [ Xavier Guimard ]
  * Remove double build
  * Fix autopkgtest
  * Bump debhelper compatibility level to 13
  * Use dh-sequence-nodejs
  * Add fix for node-schema-utils ≥ 3

 -- Xavier Guimard <yadd@debian.org>  Wed, 18 Nov 2020 16:34:51 +0100

node-yarnpkg (1.22.4-3) unstable; urgency=medium

  * Team upload
  * Embed gulp-babel 6.1.2 to allow updating node-babel7 (Closes: #958780)

 -- Pirate Praveen <praveen@debian.org>  Fri, 01 May 2020 23:10:45 +0530

node-yarnpkg (1.22.4-2) unstable; urgency=medium

  * Team upload
  * Back to unstable
  * Add dependency to ca-certificates (Closes: #952656)
  * Provide node-yarn

 -- Xavier Guimard <yadd@debian.org>  Thu, 02 Apr 2020 10:36:45 +0200

node-yarnpkg (1.22.4-1) experimental; urgency=medium

  * Team upload
  * New upstream version 1.22.4 (Closes: #952912, CVE-2020-8131)
  * Refresh patches
  * Embed component: mkdirp-classic
  * New upstream version 1.22.4
  * Use pkg-js-tools auto install

 -- Xavier Guimard <yadd@debian.org>  Mon, 30 Mar 2020 14:59:58 +0200

node-yarnpkg (1.21.1-2) unstable; urgency=medium

  * Team upload
  * Back to unstable
  * Add "Rules-Requires-Root: no"
  * Add Bug-Submit field in metadata
  * Remove outdated fields from metadata
  * Declare compliance with policy 4.5.0
  * Update lintian overrides
  * Fix HOME for autopkgtest

 -- Xavier Guimard <yadd@debian.org>  Mon, 30 Mar 2020 12:12:55 +0200

node-yarnpkg (1.21.1-2~exp1) experimental; urgency=medium

  * Fix unreasonably timestamped files from some upstream tarballs.
    Closes: #947074.
  * Require node-vinyl-fs version compatible with node-glob-stream 6.
  * Switch to gulp 4.

 -- Paolo Greppi <paolo.greppi@libpf.com>  Mon, 23 Dec 2019 09:54:18 +0100

node-yarnpkg (1.21.1-1) unstable; urgency=medium

  * New upstream version
  * Builds fine with webpack 4.7.0. Closes: #933498.
  * Repack sources with xzip

 -- Paolo Greppi <paolo.greppi@libpf.com>  Tue, 17 Dec 2019 14:28:00 +0100

node-yarnpkg (1.19.1-1) unstable; urgency=medium

  * New upstream version
  * Update embedded dependencies:
    - dnscache 1.0.1 -> 1.0.2
    - hash-for-dep 1.2.3 -> 1.5.1
    - normalize-url 4.1.0 -> 4.5.0
    - query-string 6.2.0 -> 6.8.3
    - tar-fs 1.16.3 -> 2.0.0
    - v8-compile-cache 2.0.2 -> 2.1.0
  * New embedded build dependency string-replace-loader
  * New embedded run-time dependency resolve-package-path
  * Remove force-https patch because it is now included upstream
  * Bump standard version
  * Bump debhelper compat
  * Remove debian/compat and replace debhelper by debhelper-compat

 -- Paolo Greppi <paolo.greppi@libpf.com>  Fri, 25 Oct 2019 08:24:36 +0200

node-yarnpkg (1.13.0-3) unstable; urgency=medium

  * Forces using https for the regular registries (Closes: #941354)

 -- Paolo Greppi <paolo.greppi@libpf.com>  Sun, 29 Sep 2019 14:07:45 +0200

node-yarnpkg (1.13.0-2) unstable; urgency=medium

  * De-embed strict-uri-encode component which is now available as a package
  * Use pkg-info.mk to get SOURCE_DATE_EPOCH to pass to marked-man
  * Install files in /usr/share/nodejs rather than /usr/lib/nodejs and so fix
    lintian W nodejs-module-installed-in-usr-lib

 -- Paolo Greppi <paolo.greppi@libpf.com>  Mon, 16 Sep 2019 20:05:56 +0200

node-yarnpkg (1.13.0-1) unstable; urgency=low

  * Initial release (Closes: #843021)

 -- Paolo Greppi <paolo.greppi@libpf.com>  Sat, 26 Jan 2019 08:49:57 +0100
